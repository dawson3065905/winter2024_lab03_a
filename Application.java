public class Application{
	
	//String studentName, int studentPronouns, String studentActivity, int studentSemester, int studentProgram
	
	/*
	//1
	public String name;
	//2
	public int pronounsIndex;
	//3
	public String favActivity;
	//4
	public int semester;
	//5
	public int programIndex;
	*/
	
	
	public static void main(String[] args){
		Student billSmith = new Student("Bill Smith", 0, "Bowling", 3, 1);
		
		Student jill = new Student("Jill", 1, "Reading", 1, 0);
		
		//Student gup = new Student("Gup", 2, "Attacking unsuspecting survivors", 4, 2); 
		
		Student[] section1 = new Student[3];
	
		section1[0] = billSmith;
		section1[1] = jill;
		section1[2] = new Student("Gup", 2, "Attacking unsuspecting survivors", 4, 2);
		
		System.out.println(section1[2].name);
		
		//System.out.println(billSmith.yearOfStudent());
		//System.out.println(gup.studentIntro());
		//System.out.println(Student.studentIntro());
	}
}