public class Student{
	//1
	public String name;
	//2
	public int pronounsIndex;
	//3
	public String favActivity;
	//4
	public double semester;
	//5
	public int programIndex;
	
	private String[] pronouns = {"he/him", "she/her", "they/them"};
	
	private String[] programs = {"Computer Science", "Interactive Media Arts", "English"};
	
	public Student(String studentName, int studentPronouns, String studentActivity, double studentSemester, int studentProgram){
		
		//1
		name = studentName;
		//2
		pronounsIndex = studentPronouns;
		//3
		favActivity = studentActivity;
		//4
		semester = studentSemester;
		//5
		programIndex = studentProgram;
	}
	
	public String studentIntro(){
		return "Hello!" + 
		"\r\n My name is: " + name +
		"\r\n I go by: " + pronouns[pronounsIndex] +
		"\r\n My favorite activity is: " + favActivity +
		"\r\n I am in: " + programs[programIndex];
	}
	
	public String yearOfStudent(){
		
		double yearSpent = Math.ceil(semester/2);
	
		return "I have spent " + semester + 
		" semesters here." + "\r\n So this is my " + yearSpent + " year.";
	}
}